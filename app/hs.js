import React from 'react';
import ReactDOM from 'react-dom';
import { getSampleData } from './api'

import 'highcharts'; // preload Highcharts
import ReactHighstock from 'react-highcharts/ReactHighstock.src';

// split the data set into ohlc and volume
const splitData = (input) => {
    const data = Object.keys(input).length === 0 ? [] : input;
    let ohlc = [],
        indicator = [],
        indicator2 = [],
        dataLength = data.length,
        // set the allowed units for data grouping
        groupingUnits = [[
            'week',                         // unit name
            [1]                             // allowed multiples
        ], [
            'month',
            [1, 2, 3, 4, 6]
        ]],

        i = 0;

    for (i; i < dataLength; i += 1) {
        ohlc.push([
            data[i][0], // the date
            data[i][1], // open
            data[i][2], // high
            data[i][3], // low
            data[i][4] // close
        ]);

        indicator.push([
            data[i][0], // the date
            data[i][5] // the volume as dummy indicator
        ]);

        indicator2.push([
            data[i][0], // the date
            data[i][2] - data[i][3] // high - low as dummy indicator
        ]);
    }
    return {
        ohlc: ohlc,
        indicator: indicator,
        indicator2: indicator2,
        groupingUnits: groupingUnits
    }

}

const generateConfig = (input) => {
    let config = {
        rangeSelector: {
            selected: 1
        },
        title: {
            text: 'AAPL Stock Price'
        },
        yAxis: [{
            labels: {
                align: 'right',
                x: -3
            },
            title: {
                text: 'CandleStick'
            },
            height: '40%',
            top: '30%',
            lineWidth: 2
        }, {
            labels: {
                align: 'right',
                x: -3
            },
            title: {
                text: 'Indicator1'
            },
            height: '30%',
            offset: 0,
            lineWidth: 2
        }, {
            labels: {
                align: 'right',
                x: -3
            },
            title: {
                text: 'Indiactor2'
            },
            top: '70%',
            height: '30%',
            offset: 0,
            lineWidth: 2
        }],
        series: [{
            type: 'candlestick',
            name: 'AAPL',
            data: input.ohlc,
            dataGrouping: {
                units: input.groupingUnits
            }
        }, {
            name: 'Indicator',
            data: input.indicator,
            yAxis: 1,
            dataGrouping: {
                units: input.groupingUnits
            }
        },
        {
            name: 'Test',
            data: input.indicator2,
            yAxis: 2,
            dataGrouping: {
                units: input.groupingUnits
            }
        }]
    }
    return config;
};

export default class MyChart extends React.Component {
    constructor(props) {
        super(props);
        this.state = { config: {} };
    }

    componentDidMount() {
        getSampleData().then((sampleData) => {
            // console.log('sample data:', sampleData);
            this.setState((prevState) => ({
                config: generateConfig(splitData(sampleData))
            }));
        });
    }

    render() {
        return <ReactHighstock config={this.state.config} ref="hs-chart" />;
    }
}