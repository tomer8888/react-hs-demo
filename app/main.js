import React from 'react';
import ReactDOM from 'react-dom';

import MyChart from './hs';

ReactDOM.render(
    <MyChart />,
    document.getElementById('root')
);
