import 'whatwg-fetch';
import fetchJsonp from 'fetch-jsonp'

const _parseJSON = function (response) {
    // return response.text().then(function(text) {
    //     return text ? JSON.parse(text) : {}
    // })
    return response.json();
}

// export { getJoke };
const getSampleData = function () {
    const url = 'https://www.highcharts.com/samples/data/jsonp.php?filename=aapl-ohlcv.json';
    return fetchJsonp(url, {
        mode: 'no-cors'
    }).then((resp) => _parseJSON(resp));
};

export { getSampleData }
